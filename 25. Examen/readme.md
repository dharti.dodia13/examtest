# Examen Javascript

- Travaillez dans un dossier en dehors de vos exercices habituels,
- Initialisez un dépôt GIT dès la début,
- Créez un projet sur Gitlab. Ce projet doit-être public,
- Ajoutez ce projet en Remote sur votre dépôt GIT local,
- A la fin de l'examen, vous devrez pusher vos exercices sur votre dépôt GitLab public et m'envoyer le lien de votre dépôt par message privé sur Discord,
- Chaque exercices doit-être dans un sous dossier séparé (exo1, exo2 ...),
- Vous avez le droit à la doc sur Internet et à vos anciens exercices,
- Vous devez travailler seul, pas de discord ouvert, ni d'autres canaux de discussion ;)


## Exercice 1 

Ecrire une fonction **writeHello()** qui écrit 'bonjour' dans le corps de la page HTML.
Cette fonction pourra être appelé dans la console. Si on l'appelle plusieurs fois il y aura écris 'bonjour' plusieurs fois dans la page.

## Exercice 2

Vous disposez de 2 tableaux (ci-après). 

Affichez dans une balise 'section' présente dans votre HTML le résulat de la multiplication de chaque élements du tableau 1 avec chaque élement du tableau 2.

**Exemple d'affichage attendu :**

```
4 * 65 = 260
4 * 32 = 128
4 * 456 = 1824
...
8 * 65 = 520
```

**Les tableaux :**

```js
const tabNum1 = [4,8,15,16,23,42];
const tabNum2 = [65,32,456,12,64,23,94,12];
```

## Exercice 3

Ecrire une fonction **addEventToElement(string targetName, string eventType, function listener)** qui ajoute un évènement `eventType` à la balise `targetName`.
Le but étant de s'implifier l'ajout d'un évènement. Cette fonction va donc avoir comme rôle : 

- de sélectionner l'élément du DOM sur lequel ajouter l'évènement (targetName)
- de lui ajouter un évènement (eventType) qui déclenchera la bonne fonction (listener).

Créez un bouton dans votre page et déclenchez un évènement au clique sur ce bouton avec votre fonction.
Pour testez afficher "ok" dans la console quand on clique sur le bouton.

Write a **addEventToElement(string targetName, string eventType, function listener)** function that adds an `eventType` event to the `targetName` tag.
The goal is to simplify the addition of an event. This function will therefore have the following role:

- to select the DOM element on which to add the event (targetName)
- to add an event to it (eventType) which will trigger the correct function (listener).

Create a button in your page and trigger an event when this button is clicked with your function.
To test display "ok" in the console when the button is clicked.



## Exercice 4

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Onglet</title>

    <style>

        html {
            width:100%;
            height:100%;
        }
        body {
            width:100vw;
            height:100vh;
            margin:0;
            padding:0;
        }

        nav a {
            display: inline-block;
            padding:1em;
            color:#00889d;
            border-left:1px solid #00889d;
            border-right:1px solid #00889d;
            border-top:1px solid #00889d;
            font-weight:bold;
            text-decoration: none;
            margin-right:1em;
        }
          
        nav a.current {
            color:#FFFFFF;
            background-color: #00889d;
        }

        main {
            box-sizing: border-box;
            width:100%;
            height: auto;
            padding:1em;
            border : 2px solid #00889d;
        }

        main section {
            display:none;
        }

        main section.current {
            display:block
        }

    </style>
</head>
<body>
    
    <nav>
        <a data-id="presentation" class="current" href="#">Présentation</a>
        <a data-id="information" href="#">Information</a>
        <a data-id="moncv" href="#">Mon CV</a>
        <a data-id="contact" href="#">Contacts</a>
    </nav>

    <main>
        <section id="presentation"  class="current">
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quibusdam veritatis nostrum ab! Numquam, alias consequuntur delectus debitis eum odit pariatur illo molestias dolor ab exercitationem officia iste! Velit non in qui ipsam. Expedita aspernatur dolorum, inventore laudantium consequuntur illo ipsam fuga rerum nam consectetur voluptates cupiditate dicta esse numquam odio commodi iure aperiam eum corrupti sit aut? Excepturi libero quibusdam perspiciatis, sint, cum doloremque nisi voluptatibus consequuntur repellendus delectus, minus amet tenetur sequi? Voluptas eveniet quibusdam nesciunt eaque suscipit quam provident veniam beatae, deleniti, vero repellat, doloribus magni aliquid excepturi iure rem sequi! Repellat saepe voluptate itaque, similique illo reiciendis! Fuga dicta quo saepe quaerat? Porro eos deserunt nemo obcaecati magni consequatur accusantium dolor totam doloribus, consectetur harum ex sit dolorem voluptas enim repellendus, magnam culpa mollitia adipisci dicta quam, perferendis nam minima! Mollitia molestiae autem corrupti, ratione atque voluptate quibusdam sint. A enim quo itaque nisi natus, quas tenetur esse distinctio? Vero ducimus libero vel ullam qui molestiae, tempora hic. Facere, quidem vel. Earum facilis recusandae incidunt vitae veritatis cupiditate ad? Ullam deleniti velit asperiores consequatur neque dolore? Autem, ratione corrupti natus error quod animi doloremque obcaecati iure labore porro facilis, laboriosam impedit nulla id est maiores ex necessitatibus.</p>
            <p>Aperiam hic iure magnam repellendus voluptas, accusamus assumenda in atque quasi nostrum nihil, labore quaerat, officiis dolores modi illum enim doloremque. Ex vel laborum pariatur, assumenda autem quaerat magni incidunt quisquam repellendus est, repudiandae mollitia reprehenderit doloremque molestiae. Ab dolor similique aliquam sint eum expedita recusandae iusto, architecto repellendus doloribus voluptas, in sequi, quasi tenetur excepturi ex deserunt repellat. Cumque molestiae, praesentium incidunt consequatur nisi atque! Quidem nostrum provident fugit nesciunt voluptatibus praesentium molestias reprehenderit quibusdam sit maiores, laudantium aspernatur eius dolores! Recusandae, soluta molestiae similique repudiandae asperiores aspernatur, totam a sint, doloribus eaque cumque beatae provident vitae. Nostrum eveniet animi dignissimos officia fuga? Dolorem delectus repudiandae qui enim facere consequuntur ut expedita error accusantium debitis. Ad doloremque repellat ut, blanditiis dolore ab in quidem tempore nemo maiores sint dolores, commodi officia molestias expedita voluptas adipisci eveniet sapiente ipsam laborum minus? Commodi, maxime debitis delectus, beatae quasi deleniti soluta ullam ipsum impedit, exercitationem fugit officiis modi reprehenderit et eius assumenda blanditiis veniam ipsam ea necessitatibus? Adipisci sit repellendus cumque eaque nesciunt dolorem ipsam odio saepe, dignissimos, officiis eum. Eaque illo, ab iste quam nisi obcaecati quo aperiam corporis! Voluptatibus aspernatur, nisi ad modi praesentium harum expedita incidunt odio adipisci voluptate!</p>
            <p>Dolorem, quaerat quam corporis consectetur soluta maiores molestias quod odio nesciunt consequuntur libero vel distinctio? Porro quo perspiciatis optio voluptatibus facere nobis tenetur, officiis excepturi. Ipsum quasi nesciunt non adipisci accusamus perferendis suscipit rem perspiciatis repudiandae maiores blanditiis temporibus rerum eum dolorum, repellat quod quam sequi totam harum ipsam illo inventore porro quo. Atque perspiciatis consectetur rerum soluta! Non cum beatae totam sint accusantium! Explicabo commodi cupiditate, consequatur numquam fugiat, autem voluptas ratione quos eum exercitationem, sequi aliquid officiis non eaque asperiores unde. Ut culpa eum quaerat explicabo consequuntur, accusantium consectetur, minima iusto incidunt quis deserunt perspiciatis pariatur enim, possimus est nihil omnis aperiam autem. Ratione velit perspiciatis praesentium vero non consequatur recusandae nemo aut rem, facilis qui corrupti quas? Error aut deserunt nostrum sit, fugit, nulla consequatur libero excepturi ipsa adipisci, sapiente numquam id fugiat maxime ex quis tempora eos neque earum quidem quae. Facilis vero id non porro temporibus sequi accusantium totam, suscipit earum quaerat modi consectetur dolores officia iure, et iusto illo quod nihil nesciunt recusandae repellendus animi. Esse earum atque exercitationem iste quam nam aperiam quas quae, ad, voluptatem molestias? Dolore qui magnam mollitia reprehenderit voluptas fugiat alias iste cumque. Doloremque pariatur odio deleniti illum repudiandae.</p>
        </section>

        <section id="information">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque, nemo! Dolore, adipisci. Eum minima reprehenderit rerum! Inventore, sunt quod architecto eligendi maiores temporibus est odit enim nam eius sed debitis, accusamus autem illum facere praesentium reiciendis sit eaque eos labore mollitia. Eum sit obcaecati dolorum voluptatibus optio vitae architecto iure doloribus ipsum accusantium! Modi nihil amet quod, eos voluptates consequatur dolorem debitis maxime magnam blanditiis ex ipsa similique, vel sint exercitationem dolorum, sed repudiandae odio laboriosam. Quos quasi veniam voluptatibus laborum possimus velit deleniti nemo earum ab nam, ducimus dolor nobis maxime quaerat sequi aperiam iste quo commodi. Velit, veniam minus laborum quaerat, animi doloremque nulla impedit, rerum hic temporibus inventore unde. Animi, repudiandae eaque numquam maiores saepe amet odit aperiam veniam ab reprehenderit doloribus inventore necessitatibus sit cum magnam a facilis nesciunt, iusto enim nobis. Libero modi consequuntur ut ipsum alias. Aliquid, iste! Nihil nostrum excepturi, autem veniam pariatur quod, saepe illo illum ducimus eaque rem harum perferendis alias dignissimos iusto obcaecati aspernatur, dolores reiciendis accusantium ut! Dicta cum quae vero autem perferendis voluptatem rem corporis quasi sunt, quam velit et! Nesciunt placeat quo sint doloribus voluptatibus accusamus eligendi neque aperiam amet magni repudiandae omnis praesentium, eius natus autem eaque ea dolore ad. Illum, quaerat animi! Asperiores dignissimos aliquid, nam perferendis consectetur assumenda excepturi, quis cum aut vitae dolore, vel optio voluptatum magnam exercitationem provident repudiandae nobis quod? Officia ea enim aliquam aut nostrum explicabo est laudantium maiores! Repellendus sequi consequuntur sed corporis temporibus saepe quod sapiente cum. Commodi quisquam temporibus illum, fugit molestiae repudiandae magnam assumenda aliquam inventore quidem totam nemo impedit, a sapiente possimus at. Dolore libero dolores non autem impedit molestias nostrum quia, fuga maxime, inventore totam reprehenderit sapiente voluptate. Accusantium exercitationem sint deleniti eius tempora aspernatur, fugiat eaque aperiam, non consequuntur ut eveniet officia cum?</p>
            <p>Minus quaerat ullam voluptatem, maiores, quia illo vel aliquid repudiandae harum, excepturi iure incidunt. Quod quo rem explicabo veritatis deleniti repellat adipisci incidunt numquam illo? Nam, aspernatur reprehenderit non earum ipsam facere iure et! Praesentium doloribus ab odio mollitia quibusdam! Aut, tempore eum vitae dolores exercitationem similique iste voluptatibus excepturi odio deserunt saepe quia reiciendis dignissimos dolor assumenda aliquid laudantium consectetur blanditiis error odit! Sit natus neque, eveniet earum iste, ratione dolorum ea nobis quo autem veniam aperiam quis, suscipit aut reiciendis debitis iusto optio cumque corporis cupiditate. Ut fugit consequuntur harum eligendi, quae, ipsa ullam vitae molestias error, eveniet officia similique alias? Molestiae nemo esse iusto explicabo, atque earum possimus, amet obcaecati ipsa cumque tempore voluptatem eius cum sunt. Maxime saepe cum ipsa, deserunt, error magnam at ratione quam aspernatur soluta est autem mollitia veritatis, provident blanditiis. Perspiciatis quasi a aspernatur illum. Quam vero nihil quod odit, maxime libero blanditiis harum quo dolores voluptatem nobis voluptates itaque ab deleniti pariatur deserunt quaerat saepe architecto quasi tenetur laboriosam atque, rerum neque! Quod ea porro non eos quae debitis repellendus, molestias culpa illum quisquam enim, nemo vero omnis repellat fugit harum necessitatibus ab animi cumque dolores veniam ut reiciendis. Provident animi, qui hic voluptatem molestias nobis sapiente veniam, illo minima doloribus cumque necessitatibus illum reprehenderit quasi assumenda enim. Et dolores cupiditate nostrum quam officia vero reprehenderit, sequi sapiente amet consectetur. Sapiente est ipsum ad minima, sequi voluptatibus fuga quis, veritatis consectetur, recusandae doloremque repellendus magni obcaecati voluptate. Qui voluptatum blanditiis sint pariatur nobis praesentium quo, quas recusandae mollitia perferendis ab. Cum laudantium commodi atque ducimus odio delectus dolores magnam, quia optio fuga facilis dolorum aliquid? Officia iste, adipisci nihil recusandae repudiandae dignissimos quas voluptatibus? Debitis aliquam non expedita corrupti doloribus ad aperiam ipsa recusandae odio rem, veniam aut quis eveniet voluptates.</p>  
        </section>

        <section id="moncv">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aut est animi reprehenderit? Sed, iusto porro voluptatem natus dolorum omnis nostrum!</p>
            <p>Eligendi odit dolor nulla saepe qui, nam rem maiores placeat ad beatae, vel eius architecto quibusdam error minus animi exercitationem.</p>
            <p>Officiis expedita libero accusamus dolorem non, facilis vel numquam ea quisquam qui exercitationem fuga ducimus, molestias modi! Iusto, illum delectus.</p>
            <p>Eligendi explicabo error sequi modi, voluptatibus libero corrupti! Cupiditate suscipit commodi possimus aut id tempora quae illum odit, totam magnam.</p>
            <p>Quis odit distinctio ad necessitatibus cupiditate a dolores nesciunt. Expedita voluptas ipsa velit deleniti mollitia laboriosam iusto pariatur consectetur adipisci.</p>
            <p>Quo, aut. Neque molestiae odio temporibus vero. Repudiandae officiis distinctio amet explicabo ad aspernatur a veniam, consectetur suscipit dicta? A!</p>
            <p>Ipsa repudiandae natus nostrum aperiam. Suscipit esse dicta, inventore dolor, at sit earum ut saepe cupiditate non nostrum quidem id.</p>
            <p>Quasi itaque ratione vel fugit ipsa nam nobis unde, optio, repellat quod praesentium exercitationem enim neque assumenda aliquam id consequuntur?</p>
            <p>Id at sequi possimus, perferendis eos eveniet sed, mollitia facere architecto quibusdam corrupti aliquam vel pariatur soluta placeat quisquam ipsa.</p>
            <p>Consequatur recusandae ea laudantium non repudiandae in nisi quia, possimus fuga ducimus numquam veniam. Nemo ad temporibus soluta quidem perferendis?</p>
        </section>

        <section id="contact">
            <form action="" method="POST">
                <label for="name">Nom</label><input type="text" name="name">
                <label for="email">Email</label><input type="email" name="email">
                <label for="message">Message</label><textarea name="message" cols="30" rows="10"></textarea>
                <input type="submit" value="Envoyer">
            </form>
        </section>

</body>
</html>
```

A partir de ce code HTML. Vous devez mettre en oeuvre une navigation par onglet :

- Par défaut l'onglet Présentation est activé,
- Si on clique sur un autre onglet : l'onglet se met en avant (changement de classe) et le contenu associé s'affiche. Les autres onglets se mettent en retrait (pas de classe), et les autres contenus se cachent.
- Vous ne devez rien changer à l'HTML proposé. Tout doit se passer en JS.

## Exercice 5

Intégrez 4 images au choix dans votre page HTML. 

Ces images seront affichées avec une taille de 150 x 150 pixels.

- Quand on clique sur une image, un overlay s'affiche par dessus la page avec l'image en grande taille à l'intérieur. 
- L'overlay recouvre 100% de la page (hauteur et largeur) et dispose d'un fond noir avec une opacité de 70%.
- L'image est centrée dans l'overlay et s'affiche sur 80% de la largeur de la page si elle est au format paysage (format à l'italienne), ou 80% de la hauteur si elle est au format vertical (format à la française).
- Quand on clique en dehors de l'image, l'overlay se ferme (disparait). Vous pouvez rajouter aussi un bouton de fermeture en haut à droite.

- Si vous aimez le challenge : l'utilisateur peut passer à l'image suivante ou précédent à l'aide des flèches du clavier !  
